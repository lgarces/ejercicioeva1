<%-- 
    Document   : index
    Created on : 03-abr-2021, 14:51:08
    Author     : GaCTuS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form id="frmDatos" name="frmDatos" method="post" action="DatosControlador">
            <table align="center" cellpadding="2" cellspacing="1" border="0">
                <tr>
                    <td>
                        <label for="txtNombre">Nombre Estudiante</label>
                        <input type="text" id="txtNombre" name="txtNombre" tabindex="1" maxlength="20" width="50"> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="txtNombre">Sección Estudiante</label>
                        <select id="lstSeccion" name="lstSeccion">
                            <option value="">-- Seleccione --</option>
                            <option value="6">Sección 6</option>
                            <option value="7">Sección 7</option>
                        </select> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" id="btnEnviar" name="btnEnviar" value="Enviar Datos">
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
